variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  type        = bool
  default     = true
}

variable "availability_zone_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "1"
}

variable "create_data_subnet" {
  description = "Create data subnet ressources"
  type        = bool
  default     = false
}

variable "enable_data_internet_access" {
  description = "Allow access to internet for data subnet"
  type        = bool
  default     = false
}
