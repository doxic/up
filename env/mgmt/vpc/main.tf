terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "mgmt/vpc/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

module "vpc" {
  source = "../../../modules/vpc"

  vpc_name = "mgmt"
  availability_zone_count = 2
  # single_nat_gateway = false
  #
  # create_data_subnet = true
  # enable_data_internet_access = true

  tags = {
    stack       = "mgmt"
    cost-center = 10000
    terraform   = "true"
  }
}
