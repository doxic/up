variable "scan_on_push" {
  description = "Indicates whether images are scanned after being pushed to the repository"
  type = bool
  default = true
}
