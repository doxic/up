terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.5"
}

terraform {
  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "global/ecr/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

module "ecr" {
  source = "../../../modules/ecr"

  repository_name = "hello-nginx"
  scan_on_push = var.scan_on_push

  tags = {
    stack       = "global"
    cost-center = 10000
  }
}
