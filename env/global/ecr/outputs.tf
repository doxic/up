output "repository_url" {
  value       = module.ecr.repository_url
  description = "The URL of the repository"
}
