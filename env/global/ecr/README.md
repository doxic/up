# Amazon Elastic Container Registry Example

This folder contains example [Terraform](https://www.terraform.io/) configuration that create an
[ECR](https://aws.amazon.com/ecr/) registry in an
[Amazon Web Services (AWS) account](http://aws.amazon.com/). Amazon ECR is integrated with Amazon Elastic Container Service ([https://aws.amazon.com/ecs/](ECS)).

## Pre-requisites

* You must have [Terraform](https://www.terraform.io/) installed on your computer.
* You must have an [Amazon Web Services (AWS) account](http://aws.amazon.com/).

## Quick start

Configure your [AWS access
keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) as
environment variables:

```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Specify a name for the S3 bucket and DynamoDB table in `variables.tf` using the `default` parameter:

```hcl
variable "bucket_name" {
  description = "The name of the S3 bucket. Must be globally unique."
  type        = string
  default     = "up-remote-backend-state"
}

variable "table_name" {
  description = "The name of the DynamoDB table. Must be unique in this AWS account."
  type        = string
  default     = "up-remote-backend-locks"
}
```

Deploy the code:

```
terraform init
terraform apply
```

## Manual creation
### User

Policies:
- AmazonEC2ContainerServiceFullAccess
- AmazonEC2ContainerRegistryPowerUser

AWS Management Console: https://$AWS_ACCOUNT_ID.signin.aws.amazon.com/console
```
User: ecr-remote-pusher
Access key ID: xxx
Secret access key: xxx
```


### Create a Repository
- [ ] Initialize in terraform
Create a repository with
```
aws ecr create-repository --repository-name $AWS_REPO_NAME --image-scanning-configuration scanOnPush=true --region $AWS_REGION
```

Get Amazon Resource Name (ARN) that identifies the repository (requires `jq` installed)
```
aws ecr describe-repositories --repository-names $AWS_REPO_NAME | jq -r '.[] | .[] | .repositoryArn'
```

## Direct Links
- [Amazon S3 Console](https://s3.console.aws.amazon.com/s3/home?region=eu-west-1)
- [terraform-aws-modules/terraform-aws-iam: Terraform module which creates IAM resources on AWS](https://github.com/terraform-aws-modules/terraform-aws-iam)
- [Automating ECS-EC2-Type Deployments with Terraform - Sandeep Yadav - Medium](https://medium.com/@awsyadav/automating-ecs-ec2-type-deployments-with-terraform-569863c60e69)
