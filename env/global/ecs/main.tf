terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.5"
}

terraform {
  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "global/ecs/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

module "ecs" {
  source = "../../../modules/ecs"

  cluster_name = "up-cluster"
}
