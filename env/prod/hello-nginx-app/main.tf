
provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

terraform {
  required_version = ">= 0.12, < 0.13"

  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "prod/hello-nginx-app/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

module "fargate" {
  source            = "../../../modules/fargate"

  environment = "prod"
  name = "hello-nginx"
  retention_in_days = "1"
  route53_zone = "doxic.net."
  record_name  = "onboarding.doxic.net"

  tags = {
    stack       = "prod"
    cost-center = 20001
  }

}
