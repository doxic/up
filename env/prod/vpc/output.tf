output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "The ID of the VPC"
}

output "subnet_public" {
  value       = module.vpc.subnet_public
  description = "The ID of the public subnet"
}

output "subnet_private" {
  value       = module.vpc.subnet_private
  description = "The ID of the private subnet"
}
