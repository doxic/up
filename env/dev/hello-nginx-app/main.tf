# terraform {
#   required_version = ">= 0.12, < 0.13"
# }
#
# provider "aws" {
#   region = "eu-west-1"
#
#   # Allow any 2.x version of the AWS provider
#   version = "~> 2.5"
# }
#
# terraform {
#   backend "s3" {
#     bucket         = "up-remote-backend-state"
#     key            = "dev/hello-nginx-app/terraform.tfstate"
#     region         = "eu-west-1"
#     dynamodb_table = "up-remote-backend-locks"
#     encrypt        = true
#   }
# }
#
#
# module "vpc" {
#   source   = "../../../modules/vpc"
#   az_count = 2
# }

module "security_group" {
  source = "../../../modules/security_group"
  vpc_id = module.vpc.vpc_id
}

/*
module "ecr" {
  source = "./ecr"
  region = var.region
}
*/
# 
# module "cluster" {
#   source       = "../../../modules/cluster"
#   cluster_name = "tf-ecs-cluster"
# }

# Docker images names has to be given in this block also your CPU/Memory has to be passed from this module
# CPU value	Memory value (MiB)
# 256 (.25 vCPU)	512 (0.5GB), 1024 (1GB), 2048 (2GB)
# 512 (.5 vCPU)	1024 (1GB), 2048 (2GB), 3072 (3GB), 4096 (4GB)
# 1024 (1 vCPU)	2048 (2GB), 3072 (3GB), 4096 (4GB), 5120 (5GB), 6144 (6GB), 7168 (7GB), 8192 (8GB)
# 2048 (2 vCPU)	Between 4096 (4GB) and 16384 (16GB) in increments of 1024 (1GB)
# 4096 (4 vCPU)	Between 8192 (8GB) and 30720 (30GB) in increments of 1024 (1GB)
module "task_def" {
  source         = "../../../modules/task"
  app_count      = "2"
  fargate_cpu    = "256"
  fargate_memory = "512"
  app_image      = "881887270355.dkr.ecr.eu-west-1.amazonaws.com/hello-nginx:latest"
  # This app_port is actual container port which will be used by docker
  app_port       = "80"
  log_group_name = module.cloudwatch.aws-logs
  alb_id         = module.alb.alb_arn
  vpc_id         = module.vpc.vpc_id
}

module "service" {
  source     = "../../../modules/service"
  cluster_id = module.cluster.cluster_id
  task_arn   = module.task_def.task_arn
  # App count is for no of docker containers
  app_count = "2"
  # Service count is for no of services to be provisioned on cluster#
  service_count     = "2"
  security_group_id = module.security_group.sg_id_ecs
  private_subnet    = module.vpc.private_subnet
  target_group_id   = module.alb.target_arn
  app_port          = "80"
}

# You can view scaling policy from this module
module "autoscaling" {
  source       = "../../../modules/autoscaling"
  cluster_name = module.cluster.cluster_name
  service_name = module.service.service_master
}

# Loadbalancer is on public subnet and will only connect to frontend on port 5000
module "alb" {
  source            = "../../../modules/alb"
  public_subnet     = module.vpc.public_subnet
  security_group_id = module.security_group.sg_id_lb
  tg_count          = 1
  alb_port          = "80"
  vpc_id            = module.vpc.vpc_id
  certificate_arn   = module.acm.certificate_arn
}

module "cloudwatch" {
  source            = "../../../modules/cloudwatch"
  servicename       = module.service.service_master
  cluster_name      = module.cluster.cluster_name
  asg_arn           = module.autoscaling.asg_arn
  retention_in_days = "7"
}

# module "route53_zone" {
#   source      = "./route53_zone"
#   hosted_zone = "doxic.net"
# }

module "route53" {
  source         = "../../../modules/route53"
  alb_zone       = module.alb.alb_zone
  alb_dns        = module.alb.alb_dns
  record_name    = "onboarding.doxic.net."
  hosted_zone_id = "Z0712281LU13HJVQS9YO"
}

# module "cloudflare_dns" {
#   source             = "./cloudflare_dns"
#   cloudflare_zone_id = var.cloudflare_zone_id
#   name               = module.route53.record_name
#   value              = module.alb.alb_dns
# }

module "acm" {
  source       = "../../../modules/acm"
  route53_zone = "doxic.net."
  record_name  = "onboarding.doxic.net"
}
