###############################
# Data Backends
###############################
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = var.remote_state_bucket
    key    = "${var.environment}/vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "ecr" {
  backend = "s3"

  config = {
    bucket = var.remote_state_bucket
    key    = "global/ecr/terraform.tfstate"
  }
}

data "terraform_remote_state" "ecs" {
  backend = "s3"

  config = {
    bucket = var.remote_state_bucket
    key    = "global/ecs/terraform.tfstate"
  }
}

locals {
  http_port    = 80
  https_port = 443
  app_count = 2
}

###############################
# Application Load Balancer
###############################

# TODO: Not really bound to ECS, move to own module
resource "aws_alb" "ecs" {
  name            = "alb-ecs-${var.environment}"
  subnets         = data.terraform_remote_state.vpc.outputs.subnet_public.*.id
  security_groups = [aws_security_group.alb.id]

  tags = merge(
    {
      "Name" = "alb-ecs-${var.environment}"
      terraform   = "true"
    },
    var.tags,
  )
}

# ALB Security group: Restrict access to ALB
resource "aws_security_group" "alb" {
  name        = "securitygroup-alb-${var.environment}"
  description = "controls access to the ALB"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id

  # TODO: Bad style, move to ressources
  ingress {
    protocol    = "tcp"
    from_port   = local.http_port
    to_port     = local.http_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = local.https_port
    to_port     = local.https_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = "securitygroup-alb-${var.environment}"
      terraform   = "true"
    },
    var.tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

# Traffic to the ECS cluster should only come from the ALB
resource "aws_security_group" "ecs_tasks" {
  name        = "securitygroup-alb-ecs_task-${var.environment}"
  description = "allow inbound access from the ALB only"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = local.http_port
    to_port     = local.http_port
    security_groups = [aws_security_group.alb.id]
  }

  ingress {
    protocol    = "tcp"
    from_port   = local.https_port
    to_port     = local.https_port
    security_groups = [aws_security_group.alb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = "securitygroup-alb-ecs_task-${var.environment}"
      terraform   = "true"
    },
    var.tags,
  )

  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_alb_target_group" "app" {
  name        = var.name
  port        = local.http_port
  protocol    = "HTTP"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/"
  }

  tags = merge(
    {
      "Name" = var.name
      terraform   = "true"
    },
    var.tags,
  )
}

# Redirect http traffic to https
resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_alb.ecs.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  depends_on        = [aws_alb.ecs]
}

# TODO: Create own ssl_negotiation_policy
# https://www.terraform.io/docs/providers/aws/r/lb_ssl_negotiation_policy.html

# Redirect https traffic from the ALB to the target group
resource "aws_alb_listener" "https" {
  load_balancer_arn = aws_alb.ecs.id
  port              = "443"
  protocol          = "HTTPS"
  # Using the output from the validation resource ensures that Terraform will wait for ACM to validate the certificate before resolving its ARN.
  certificate_arn   = aws_acm_certificate_validation.default.certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }

  depends_on        = [aws_alb.ecs]
}

###############################
# DNS records & Certificates
###############################

data "aws_route53_zone" "external" {
  name = var.route53_zone
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.external.zone_id
  name    = var.record_name
  type    = "A"

  alias {
    name                   = aws_alb.ecs.dns_name
    zone_id                = aws_alb.ecs.zone_id
    evaluate_target_health = true
  }

}

resource "aws_acm_certificate" "default" {
  domain_name       = var.record_name
  validation_method = "DNS"

  tags = merge(
    {
      "Name" = "${var.name}-${var.environment}"
      terraform   = "true"
    },
    var.tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "validation" {
  name    = aws_acm_certificate.default.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.default.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = [aws_acm_certificate.default.domain_validation_options.0.resource_record_value]
  ttl     = "60"
}

# Wait for the newly created certificate to become valid
resource "aws_acm_certificate_validation" "default" {
  certificate_arn = aws_acm_certificate.default.arn
  validation_record_fqdns = [
    aws_route53_record.validation.fqdn,
  ]
}

###############################
# Fargate Task Definition
###############################

data "template_file" "container_definition" {
  template = file("${path.module}/container_definition.json")

  vars = {
    name = var.name
    app_image = "${data.terraform_remote_state.ecr.outputs.repository_url}:latest"
    fargate_cpu = var.fargate_cpu
    fargate_memory = var.fargate_memory
    log_group_name = aws_cloudwatch_log_group.aws-logs.name
    aws_region = var.aws_region
    container_port = local.http_port
    host_port = local.http_port
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.name}-task-definition-${var.environment}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory

  container_definitions = data.template_file.container_definition.rendered

  tags = merge(
    {
      "Name" = "${var.name}-task-definition-${var.environment}"
      terraform   = "true"
    },
    var.tags,
  )
}

resource "aws_ecs_service" "app" {
  name            = "${var.name}-service-${var.environment}"
  cluster         = data.terraform_remote_state.ecs.outputs.cluster_name
  task_definition = aws_ecs_task_definition.this.arn
  desired_count   = local.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.ecs_tasks.id]
    subnets         = data.terraform_remote_state.vpc.outputs.subnet_private.*.id
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.app.id
    container_name   = var.name
    container_port   = local.http_port
  }

  # The new ARN and resource ID format must be enabled to add tags to the service.
  # tags = merge(
  #   {
  #     "Name" = "${var.name}-service-${var.environment}"
  #     terraform   = "true"
  #   },
  #   var.tags,
  # )


  lifecycle {
    # Allow external changes without Terraform plan difference
    ignore_changes = [desired_count]
    create_before_destroy = true
  }

  depends_on = [aws_alb_listener.https, aws_iam_role_policy_attachment.ecs_task_execution_role, aws_ecs_task_definition.this]
}

# ECS task execution role data
data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid = ""
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# ECS task execution role
resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "ecs_task_execution_role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

# ECS task execution role policy attachment
resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

###############################
# Cloudwatch
###############################

# CloudWatch alarm that triggers the autoscaling up policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = "${var.name}-${var.environment}-cpu-utilization-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    ClusterName = data.terraform_remote_state.ecs.outputs.cluster_name
    ServiceName = aws_ecs_service.app.name
  }

  alarm_description = "This metric monitors average CPU usage and triggers when too high"
  alarm_actions     = ["${aws_sns_topic.autoscaling_updates.arn}", "${aws_appautoscaling_policy.service_policy_up.arn}"]

  tags = merge(
    {
      "Name" = "${var.name}-${var.environment}-cpu-utilization-high"
      terraform   = "true"
    },
    var.tags,
  )
}

# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  alarm_name          = "${var.name}-${var.environment}-cpu-utilization-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "10"

  dimensions = {
    ClusterName = data.terraform_remote_state.ecs.outputs.cluster_name
    ServiceName = aws_ecs_service.app.name
  }

  alarm_description = "This metric monitors average CPU usage and triggers when too low"
  alarm_actions     = ["${aws_sns_topic.autoscaling_updates.arn}", "${aws_appautoscaling_policy.service_policy_down.arn}"]

  tags = merge(
    {
      "Name" = "${var.name}-${var.environment}-cpu-utilization-low"
      terraform   = "true"
    },
    var.tags,
  )
}

resource "aws_cloudwatch_log_group" "aws-logs" {
  name              = "awslogs-ecs"
  retention_in_days = var.retention_in_days
}

resource "aws_sns_topic" "autoscaling_updates" {
  name = "ecs-updates-topic"
}

###############################
# Autoscaling
###############################
resource "aws_appautoscaling_target" "ecs" {
  service_namespace  = "ecs"
  max_capacity       = 10
  min_capacity       = 2
  resource_id        = "service/${data.terraform_remote_state.ecs.outputs.cluster_name}/${aws_ecs_service.app.name}"
  #role_arn           = aws_iam_role.asgrole.arn
  scalable_dimension = "ecs:service:DesiredCount"
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "service_policy_up" {
  name               = "${var.name}-${var.environment}-scale-up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "service_policy_down" {
  name               = "${var.name}-${var.environment}-scale-down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}