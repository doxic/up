variable "remote_state_bucket" {
  description = "The name of the S3 bucket used for the remote state storage"
  type        = string
  default     = "up-remote-backend-state"
}

variable "aws_region" {
  description = "Default AWS region"
  type        = string
  default     = "eu-west-1"
}

variable "environment" {
  description = "Environment used for this module"
  type        = string
}

variable "name" {
  description = "A unique name for your application"
  type = string
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  type = number
  default     = "256"
}
variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  type = number
  default     = "512"
}

variable "retention_in_days" {
  description = "Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  type        = string
  default     = "0"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}


variable "route53_zone" {}
variable "record_name" {}


# app_image
# fargate_memory
# name
# log_group_name
# aws_region
# container_port
# host_port
