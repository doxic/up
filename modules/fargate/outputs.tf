# output "alb_arn" {
#   value = aws_alb.ecs.arn
# }
# output "target_arn" {
#   value = aws_alb_target_group.app.arn
# }

# output "alb_dns" {
#   value = aws_alb.ecs.dns_name
# }
# output "alb_zone" {
#   value = aws_alb.ecs.zone_id
# }


# output "certificate_arn" {
#   value = aws_acm_certificate_validation.default.certificate_arn
# }


# output "service_master" {
#   value = aws_ecs_service.main-master.name
# }


# # Output Task resource arn
# output "task_arn" {
#   value = aws_ecs_task_definition.this.arn
# }


output "cluster_name" {
  value = data.terraform_remote_state.ecs.outputs.cluster_name
}

output "service_name" {
  value = aws_ecs_service.app.name
}

output "task_definition_name" {
  value = aws_ecs_task_definition.this.family
}
