resource "aws_ecr_repository" "this" {

  name = var.repository_name

  # identifying software vulnerabilities in container images.
  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }

  # Prevent accidental deletion of this S3 bucket
  lifecycle {
    prevent_destroy = true
  }

  tags = merge(
    {
      "Name" = format("%s", var.repository_name)
      "terraform"   = "true"
    },
    var.tags,
  )
}
