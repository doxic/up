variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "scan_on_push" {
  description = "Indicates whether images are scanned after being pushed to the repository"
  type = bool
  default = true
}

variable "repository_name" {
  description = "Name of the repository."
  type        = string
}
