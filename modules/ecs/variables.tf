variable "cluster_name" {
  description = "The name of the cluster (up to 255 letters, numbers, hyphens, and underscores)"
  type = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
