resource "aws_ecs_cluster" "this" {
  name = var.cluster_name

  tags = merge(
    {
      "Name" = format("%s", var.cluster_name)
      "terraform"   = "true"
    },
    var.tags,
  )
}
