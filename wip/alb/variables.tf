variable "public_subnet" {
  type        = list
  description = "Enter public subnets for ALB"
}

variable "vpc_id" {
  type = string
}

variable "security_group_id" {
  type = string
}

variable "tg_count" {
  type = string
}

variable "certificate_arn" {}
variable "alb_port" {}
