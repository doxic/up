### ALB
# Using the output from the validation resource ensures that Terraform will wait for ACM to validate the certificate before resolving its ARN.
resource "aws_alb" "main" {
  depends_on      = [var.public_subnet, var.vpc_id]
  name            = "ECS-ALB"
  subnets         = [var.public_subnet.0, var.public_subnet.1]
  security_groups = [var.security_group_id]
}

resource "aws_alb_target_group" "app" {

  name        = "ecs-app"
  port        = var.alb_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"
  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/"
    port                = var.alb_port
  }

  depends_on  = [aws_alb.main]
}

# Redirect all traffic from the ALB to the target group
# resource "aws_alb_listener" "nginx" {
#   depends_on        = [aws_alb.main]
#   load_balancer_arn = aws_alb.main.id
#   port              = var.alb_port
#   protocol          = "HTTP"
#
#   default_action {
#     target_group_arn = aws_alb_target_group.app.id
#     type             = "forward"
#   }
# }

# TODO: Create own ssl_negotiation_policy
# https://www.terraform.io/docs/providers/aws/r/lb_ssl_negotiation_policy.html

resource "aws_alb_listener" "nginx" {
  depends_on        = [aws_alb.main]
  load_balancer_arn = aws_alb.main.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
resource "aws_alb_listener" "nginx_ssl" {
  depends_on        = [aws_alb.main]
  load_balancer_arn = aws_alb.main.id
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }
}



output "alb_arn" {
  value = aws_alb.main.arn
}
output "target_arn" {
  value = aws_alb_target_group.app.arn
}

output "alb_dns" {
  value = aws_alb.main.dns_name
}
output "alb_zone" {
  value = aws_alb.main.zone_id
}
