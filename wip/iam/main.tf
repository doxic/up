terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "global/iam/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

# Provides an IAM user
resource "aws_iam_user" "this" {
  count = var.create_user ? 1 : 0

  name                 = var.name
  force_destroy        = var.force_destroy
  permissions_boundary = var.permissions_boundary
  tags                 = var.tags
}

# Creates a password for the specified user, giving the user the ability to access AWS Management Console.
resource "aws_iam_user_login_profile" "this" {
  count = var.create_user && var.create_iam_user_login_profile ? 1 : 0

  user                    = aws_iam_user.this[0].name
  pgp_key                 = var.pgp_key
  password_length         = var.password_length
  password_reset_required = var.password_reset_required
}

# This is a set of credentials that allow API requests to be made as an IAM user.
resource "aws_iam_access_key" "this" {
  count = var.create_user && var.create_iam_access_key ? 1 : 0

  user = aws_iam_user.this[0].name
}

# Uploads an SSH public key and associates it with the specified IAM user.
resource "aws_iam_user_ssh_key" "this" {
  count = var.create_user && var.upload_iam_user_ssh_key ? 1 : 0

  username   = aws_iam_user.this[0].name
  encoding   = var.ssh_key_encoding
  public_key = var.ssh_public_key
}
