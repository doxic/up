# AWS IAM user

## Initializing with GPG
GPG is an open-source implementation of the OpenPGP standard and is available on nearly every platform. For more information, please see the [GPG manual](https://gnupg.org/gph/en/manual.html).

create a new PGP key
```
gpg --gen-key
```

Export public key base64 encoded
```
gpg --export 05F71E3347041BD1 | base64 | tr -d '\n'
```

The encrypted password may be decrypted using the command line
```
echo "" | base64 --decode | gpg -dq
```

### Keybase approach
Need to install Keybase in our local
need to create Keybase key by using "keybase pgp gen "
then give the reference of this Keybase key in your terraform code Keybase:username_of_keybase
Then terraform apply
Then we need to get the decrypted password "terraform output password | base64 --decode | keybase pgp decrypt"
