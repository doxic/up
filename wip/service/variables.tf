variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}
variable "app_port" {
  type = string
}
variable "cluster_id" {
  type = string
}
variable "target_group_id" {
  type = string
}
variable "private_subnet" {
  type = list
}
variable "service_count" {
  description = "Number of serivces with diff docker images count"
  default     = 2
}
variable "task_arn" {
  type        = string
  description = "Task desfinition arn"
}
variable "security_group_id" {
  type = string
}
