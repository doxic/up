resource "aws_ecs_service" "main-master" {
  depends_on      = [var.task_arn, var.target_group_id]
  name            = "tf-ecs-service-master"
  cluster         = var.cluster_id
  task_definition = var.task_arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [var.security_group_id]
    subnets         = var.private_subnet
  }

  load_balancer {
    target_group_arn = var.target_group_id
    container_name   = "app-nginx"
    container_port   = var.app_port
  }

  # Allow external changes without Terraform plan difference
  lifecycle {
    ignore_changes = [desired_count]
  }

  #  depends_on = [
  #    "aws_alb_listener.front_end",
  #  ]
}
/*
resource "aws_ecs_service" "main-worker" {
  depends_on = [var.task_arn,var.target_group_id]
  name            = "tf-ecs-service-worker"
  cluster         = var.cluster_id
  task_definition = element(var.task_arn.*,1)
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [var.security_group_id]
    subnets         = ["${element(var.private_subnet.*,1)}"]
  }


}
*/
output "service_master" {
  value = aws_ecs_service.main-master.name
}
