resource "cloudflare_record" "dns" {
  zone_id = var.cloudflare_zone_id
  name    = var.name
  value   = var.value
  type    = "CNAME"
  proxied = false
}
