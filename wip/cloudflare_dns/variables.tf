variable "cloudflare_zone_id" {
  description = "Cloudflare Domain Zone"
  type        = string
}

variable "name" {
  description = "Name of the record"
  type        = string
}

variable "value" {
  description = "Value of the record"
  type        = string
}
