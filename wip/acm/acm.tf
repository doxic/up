data "aws_route53_zone" "external" {
  name = var.route53_zone
}

resource "aws_acm_certificate" "default" {
  domain_name       = var.record_name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "validation" {
  name    = aws_acm_certificate.default.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.default.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = [aws_acm_certificate.default.domain_validation_options.0.resource_record_value]
  ttl     = "60"
}

# Wait for the newly created certificate to become valid
resource "aws_acm_certificate_validation" "default" {
  certificate_arn = aws_acm_certificate.default.arn
  validation_record_fqdns = [
    aws_route53_record.validation.fqdn,
  ]
}

output "certificate_arn" {
  value = aws_acm_certificate_validation.default.certificate_arn
}
