variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  type        = string
  #default     = "adongy/hostname-docker:latest"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  type        = string
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

variable "vpc_id" {
  type = string
}
variable "alb_id" {
  type = string
}
variable "log_group_name" {
  type = string
}

variable "familyname" {
  type    = string
  default = "nginx"
}

variable "aws_region" {
  type    = string
  default = "eu-west-1"
}
