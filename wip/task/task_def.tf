#Create task def for nginx
resource "aws_ecs_task_definition" "nginx" {
  depends_on               = [var.vpc_id, var.alb_id]
  family                   = var.familyname
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = aws_iam_role.role.arn
  execution_role_arn       = aws_iam_role.role.arn
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.fargate_cpu},
    "image": "${var.app_image}",
    "memory": ${var.fargate_memory},
    "name": "app-nginx",
    "networkMode": "awsvpc",
    "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "${var.log_group_name}",
                    "awslogs-region": "${var.aws_region}",
                    "awslogs-stream-prefix": "/app"
                }
            },
    "vars": { "sample": "test"},
    "portMappings": [
      {
        "containerPort": ${var.app_port},
        "hostPort": ${var.app_port}
      }
    ]
  }
]
DEFINITION
}


resource "aws_iam_role" "role" {
  name = "ecrfiver"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "ecr-attach" {
  name       = "ecr-attachment-fiver"
  roles      = ["${aws_iam_role.role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}
resource "aws_iam_policy_attachment" "ecr-exec-attach" {
  name       = "test-attachment-fiver"
  roles      = ["${aws_iam_role.role.name}"]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}



# Output Task resource arn
output "task_arn" {
  value = aws_ecs_task_definition.nginx.arn
}
