#================= Autoscaling =====================================
resource "aws_appautoscaling_target" "ecs_scaling" {
  depends_on         = [var.cluster_name, var.service_name]
  max_capacity       = 10
  min_capacity       = 2
  resource_id        = "service/${var.cluster_name}/${var.service_name}"
  role_arn           = aws_iam_role.asgrole.arn
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_policy_up" {
  name               = "scale-up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_scaling.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_scaling.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_scaling.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}

resource "aws_appautoscaling_policy" "ecs_policy_down" {
  name               = "scale-down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.ecs_scaling.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_scaling.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_scaling.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}

# Target tracking policy
/*
resource "aws_appautoscaling_policy" "ecs_targettracking" {
  count = 2
  name               = "ECSServiceAverageCPUUtilization:${count.index}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = element(aws_appautoscaling_target.ecs_scaling.*.resource_id,count.index)
  scalable_dimension = element(aws_appautoscaling_target.ecs_scaling.*.scalable_dimension,count.index)
  service_namespace  = element(aws_appautoscaling_target.ecs_scaling.*.service_namespace,count.index)

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 50
  }
}*/
###############

resource "aws_iam_role" "asgrole" {
  name = "asg-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "autoscale-attach" {
  name       = "autoscale-attachment"
  roles      = ["${aws_iam_role.asgrole.name}"]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}
# Output autoscaling arn
output "asg_arn" {
  value = aws_appautoscaling_policy.ecs_policy_up.arn
}
