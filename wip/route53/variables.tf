variable "alb_zone" {
  type = string
}
variable "alb_dns" {
  type = string
}
variable "hosted_zone_id" {
  type = string
}
variable "record_name" {
  type = string
}
