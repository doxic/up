resource "aws_route53_record" "www" {
  zone_id = var.hosted_zone_id
  name    = var.record_name
  type    = "A"

  alias {
    name                   = var.alb_dns
    zone_id                = var.alb_zone
    evaluate_target_health = true
  }
}

output "record_name" {
  value = aws_route53_record.www.name
}
