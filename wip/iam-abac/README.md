# AWS Attribute-based access control

This folder contains [Terraform](https://www.terraform.io/) configuration that creates an Attribute-based access control ([https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction_attribute-based-access-control.html](ABAC)) authorization strategy that defines permissions enforcing least privilege.

## Pre-requisites

* You must have [Terraform](https://www.terraform.io/) installed on your computer.
* You must have an [Amazon Web Services (AWS) account](http://aws.amazon.com/).

## Reference
- [AWS Services That Work with IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html)
- [Implement an Effective AWS Resource Tagging Strategy](https://d1.awsstatic.com/whitepapers/aws-tagging-best-practices.pdf)


## Tutorial ABAC
https://docs.aws.amazon.com/IAM/latest/UserGuide/tutorial_attribute-based-access-control.html

1. To create a customer managed policy [AWS CLI](https://docs.aws.amazon.com/cli/latest/reference/iam/create-policy.html): `aws iam create-policy --policy-name access-assume-role --policy-document file://access-assume-policy.json`

2. [Create IAM users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html#id_users_create_cliwpsapi), attach access-assume-role permissions policy, and add tags:

```shell
aws iam create-user --user-name access-Arnav-peg-eng

aws iam attach-user-policy --user-name access-Arnav-peg-eng --policy-arn "arn:aws:iam::881887270355:policy/access-assume-role"

aws iam tag-user --user-name access-Arnav-peg-eng --tags '[{"Key": "access-project", "Value": "peg"}, {"Key": "access-team", "Value": "eng"}, {"Key": "cost-center", "Value": "987654"}]'

aws iam list-user-tags --user-name access-Arnav-peg-eng
```

3. Create the ABAC Policy
