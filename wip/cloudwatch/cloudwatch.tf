resource "aws_cloudwatch_metric_alarm" "cpualerts" {
  alarm_name          = "ECS-High_CPU"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    Name  = "ServiceName"
    Value = "${var.servicename}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_sns_topic.autoscaling_updates.arn}", "${var.asg_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "ecs-alert_High-MemReservation" {
  alarm_name          = "ECS-High_MemResv"
  comparison_operator = "GreaterThanOrEqualToThreshold"

  period              = "60"
  evaluation_periods  = "1"
  datapoints_to_alarm = 1

  statistic         = "Average"
  threshold         = "80"
  alarm_description = ""

  metric_name = "MemoryReservation"
  namespace   = "AWS/ECS"
  dimensions = {
    ClusterName = "${var.cluster_name}"
  }

  actions_enabled           = true
  insufficient_data_actions = []
  ok_actions                = []
  alarm_actions             = ["${aws_sns_topic.autoscaling_updates.arn}", "${var.asg_arn}"]
}

resource "aws_cloudwatch_log_group" "aws-logs" {
  name              = "awslogs-ecs"
  retention_in_days = var.retention_in_days
}

resource "aws_sns_topic" "autoscaling_updates" {
  name = "ecs-updates-topic"
}

output "aws-logs" {
  value = aws_cloudwatch_log_group.aws-logs.name
}
