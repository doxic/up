variable "aws_region" {
  type = string
}

variable "aws_access_key" {
  description = "AWS API access key"
}

variable "aws_secret_key" {
  description = "AWS API secret key"
}

variable "cloudflare_email" {
  description = "Cloudflare email"
}

variable "cloudflare_token" {
  description = "Cloudflare API token"
}
