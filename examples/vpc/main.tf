terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-west-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket         = "up-remote-backend-state"
    key            = "mgmt/vpc/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "up-remote-backend-locks"
    encrypt        = true
  }
}

module "wireguard" {
  source = "../../../modules/wireguard"

module "vpc" {
  source   = "../../../modules/vpc"
  az_count = 2
}
