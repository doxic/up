# Terraform dtcloud

## Prerequisites
## Getting started
Installation is done with `pip` using Python 3 and venv. See the Links section for additional help installing Python 3.

Install git, git-crypt, python3 and pip3
```shell
$ sudo apt update && apt install git python3 python3-pip python3-setuptools git-crypt
```

Clone this repository locally
```shell
$ git clone https://gitlab.com/doxic/dtcloud.git
$ cd terraform
```

Test for existing awscli and remove it
```
aws --version
pip3 uninstall awscli
```

Create a virtual environment
```shell
$ python3 -m venv .venv
```

Activate virtual environment.
```shell
$ source .venv/bin/activate
```

Install project dependencies
```shell
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

Install Terraform
```shell
wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
unzip terraform*.zip
rm terraform*.zip
chmod +x terraform
sudo mv terraform /usr/bin
```

## Conventions
### Ressources
[Standard Module Structure - Terraform by HashiCorp](https://www.terraform.io/docs/modules/index.html#standard-module-structure)

### Remote Backends
[Backend Type: s3 - Terraform by HashiCorp](https://www.terraform.io/docs/backends/types/s3.html)

## ECR
### Ressources
- [terraform-aws-modules/terraform-aws-iam: Terraform module which creates IAM resources on AWS](https://github.com/terraform-aws-modules/terraform-aws-iam)
- [Automating ECS-EC2-Type Deployments with Terraform - Sandeep Yadav - Medium](https://medium.com/@awsyadav/automating-ecs-ec2-type-deployments-with-terraform-569863c60e69)

### User

Policies:
- AmazonEC2ContainerServiceFullAccess
- AmazonEC2ContainerRegistryPowerUser

AWS Management Console: https://$AWS_ACCOUNT_ID.signin.aws.amazon.com/console
```
User: ecr-remote-pusher
Access key ID: xxx
Secret access key: xxx
```


### Create a Repository
- [ ] Initialize in terraform
Create a repository with
```
aws ecr create-repository --repository-name $AWS_REPO_NAME --image-scanning-configuration scanOnPush=true --region $AWS_REGION
```

Get Amazon Resource Name (ARN) that identifies the repository (requires `jq` installed)
```
aws ecr describe-repositories --repository-names $AWS_REPO_NAME | jq -r '.[] | .[] | .repositoryArn'
```

### Authenticate to Registry
Authenticate Docker to an Amazon ECR registry with get-login-password (valid for 12 hours)
```
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
```

Pushing an Image
```
docker build -t $AWS_REPO_NAME .
docker tag $AWS_REPO_NAME":latest" $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$AWS_REPO_NAME":latest"
docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$AWS_REPO_NAME":latest"
```

### Replace image
List ressources in terraform with `terraform state list`

Taint desired object and plan / apply changes
```
terraform taint module.task_def.aws_ecs_task_definition.nginx
terraform plan
terraform apply
```

### Pipeline: Replace image
[UpdateService - Amazon Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_UpdateService.html)
```
aws ecs update-service --cluster tf-ecs-cluster --service tf-ecs-service-master --task-definition nginx
```


## Route53
[Prevent Terraform from Recreating or Deleting Resource · Coderbook](https://coderbook.com/@marcus/prevent-terraform-from-recreating-or-deleting-resource/)

Create a hosted zone
```
aws route53 create-hosted-zone --name dominic.dev --caller-reference $(date +%F-%H:%M)
```

Get hosted zone id
```
aws route53 list-hosted-zones-by-name | jq --arg name "dominic.dev." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id'
```

Get list of the authoritative name servers for a hosted zone
```
aws route53 get-hosted-zone --id /hostedzone/Z06720902QCKL6590PUKG  | jq -r ' .DelegationSet | .NameServers'
```

## ALB
### Ressources
- [Choosing Between an ELB and an ALB on AWS | Sumo Logic](https://www.sumologic.com/blog/aws-elb-alb/)
- [Create an HTTPS Listener for Your Application Load Balancer - Elastic Load Balancing](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html)
- [azavea/terraform-aws-acm-certificate: A Terraform module to create an Amazon Certificate Manager (ACM) certificate with Route 53 DNS validation.](https://github.com/azavea/terraform-aws-acm-certificate)
- [Provisioning ACM Certificates on AWS with Terraform | Azavea](https://www.azavea.com/blog/2018/07/16/provisioning-acm-certificates-on-aws-with-terraform/)
- [Setting up an SSL certificate using AWS and Terraform](https://medium.com/@Markus.Hanslik/setting-up-an-ssl-certificate-using-aws-and-terraform-198c6fb90743)
- [SSL Negotiation Configurations for Classic Load Balancers - Elastic Load Balancing](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-ssl-security-policy.html)

### HTTPS Listener


## ECS (Fargate)
### Tasks
| CPU value      | Memory value (MiB)                                                                 |
|:---------------|:-----------------------------------------------------------------------------------|
| 256 (.25 vCPU) | 512 (0.5GB), 1024 (1GB), 2048 (2GB)                                                |
| 512 (.5 vCPU)  | 1024 (1GB), 2048 (2GB), 3072 (3GB), 4096 (4GB)                                     |
| 1024 (1 vCPU)  | 2048 (2GB), 3072 (3GB), 4096 (4GB), 5120 (5GB), 6144 (6GB), 7168 (7GB), 8192 (8GB) |
| 2048 (2 vCPU)  | Between 4096 (4GB) and 16384 (16GB) in increments of 1024 (1GB)                    |
| 4096 (4 vCPU)  | Between 8192 (8GB) and 30720 (30GB) in increments of 1024 (1GB)                    |

### Logs
#### Ressources
- [Using the awslogs Log Driver - Amazon Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_awslogs.html)
- [A simple way to manage log messages from containers: CloudWatch Logs | cloudonaut](https://cloudonaut.io/a-simple-way-to-manage-log-messages-from-containers-cloudwatch-logs/)
- [Using CloudWatch Logs with Container Instances - Amazon Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_cloudwatch_logs.html)



## IPsec VPN

[Enable IKEv2 VPN Tunnel Negotiation with AWS VPN](https://aws.amazon.com/premiumsupport/knowledge-center/vpc-enable-ikev2-vpn-tunnel-negotiation/)
[edgeos_config – Manage EdgeOS configuration on remote device — Ansible Documentation](https://docs.ansible.com/ansible/latest/modules/edgeos_config_module.html)
[EdgeRouter - Route-Based Site-to-Site VPN to Azure (VTI over IKEv2/IPsec) – Ubiquiti Networks Support and Help Center](https://help.ui.com/hc/en-us/articles/115012305347-EdgeRouter-Route-Based-Site-to-Site-VPN-to-Azure-VTI-over-IKEv2-IPsec-)
[EdgeRouter - Route-Based Site-to-Site IPsec VPN – Ubiquiti Networks Support and Help Center](https://help.ui.com/hc/en-us/articles/115011377588-EdgeRouter-Route-Based-Site-to-Site-IPsec-VPN)

## Testing
https://medium.com/better-programming/introduction-to-locust-an-open-source-load-testing-tool-in-python-2b2e89ea1ff
TODO: Locust

## Cleanup
Install Cloud Nuke
```shell
wget https://github.com/gruntwork-io/cloud-nuke/releases/download/v0.1.18/cloud-nuke_linux_amd64
mv cloud-nuke* cloud-nuke
chmod +x cloud-nuke
sudo mv cloud-nuke /usr/bin
```

Nuke AWS region `eu-west-1` without S3 (containing remote state of terraform)
```
cloud-nuke aws --region eu-west-1 --exclude-resource-type s3
```
